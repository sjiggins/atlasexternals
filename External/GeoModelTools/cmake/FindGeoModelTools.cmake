# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Locate the GeoModelTools external package.
#
# Defines:
#  GEOMODELTOOLS_FOUND
#  GEOMODELTOOLS_INCLUDE_DIR
#  GEOMODELTOOLS_INCLUDE_DIRS
#  GEOMODELTOOLS_<component>_FOUND
#  GEOMODELTOOLS_<component>_LIBRARY
#  GEOMODELTOOLS_LIBRARIES
#  GEOMODELTOOLS_LIBRARY_DIRS
#
# The user can set GEOMODELTOOLS_ATROOT to guide the script.
#

# Include the helper code:
include( AtlasInternals )

# Declare the module:
atlas_external_module( NAME GeoModelTools
   INCLUDE_SUFFIXES include
   INCLUDE_NAMES GeoModelXMLParser/XMLHandler.h
                 ExpressionEvaluator/ExpressionEvaluator.h
   LIBRARY_SUFFIXES lib
   DEFAULT_COMPONENTS ExpressionEvaluator GeoModelJSONParser
                      GeoModelXMLParser )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( GeoModelTools DEFAULT_MSG
   GEOMODELTOOLS_INCLUDE_DIR GEOMODELTOOLS_INCLUDE_DIRS
   GEOMODELTOOLS_LIBRARIES )
mark_as_advanced( GEOMODELTOOLS_FOUND GEOMODELTOOLS_INCLUDE_DIR
   GEOMODELTOOLS_INCLUDE_DIRS GEOMODELTOOLS_LIBRARIES
   GEOMODELTOOLS_LIBRARY_DIRS )
