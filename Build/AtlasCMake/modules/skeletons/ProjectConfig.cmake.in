# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Configuration file for an ATLAS CMake release installation.
# It defines the following variables:
#
#  @CMAKE_PROJECT_NAME@_ROOT_DIR     - Root directory for the release
#  @CMAKE_PROJECT_NAME@_INCLUDE_DIR  - Include directory for the release
#  @CMAKE_PROJECT_NAME@_INCLUDE_DIRS - Include directory for the release
#  @CMAKE_PROJECT_NAME@_LIBRARY_DIR  - Library directory for the release
#  @CMAKE_PROJECT_NAME@_LIBRARY_DIRS - Library directory for the release
#  @CMAKE_PROJECT_NAME@_RUNTIME_DIR  - Runtime directory for the release
#  @CMAKE_PROJECT_NAME@_MODULE_DIR   - Directory holding CMake modules
#  @CMAKE_PROJECT_NAME@_PYTHON_DIR   - Directory holding python code
#
# Note however that most of the time you should not be using any of these
# variables directly, but should rather use the ROOTCORE_PROJECT(...) function
# to set up a work area that compiles against this release.
#

# Get some path names:
get_filename_component( _thisdir@CMAKE_PROJECT_NAME@
   "${CMAKE_CURRENT_LIST_FILE}" PATH )
get_filename_component( _releasedir@CMAKE_PROJECT_NAME@
   "${_thisdir@CMAKE_PROJECT_NAME@}" PATH )

# Set the root directory variable:
get_filename_component( @CMAKE_PROJECT_NAME@_ROOT_DIR
   "${_releasedir@CMAKE_PROJECT_NAME@}" ABSOLUTE )

# Print what project/release was found just now:
if( NOT @CMAKE_PROJECT_NAME@_FIND_QUIETLY )
   message( STATUS "Found @CMAKE_PROJECT_NAME@: "
      "${@CMAKE_PROJECT_NAME@_ROOT_DIR} (version: @CMAKE_PROJECT_VERSION@)" )
endif()

# Set the include directory variable:
get_filename_component( @CMAKE_PROJECT_NAME@_INCLUDE_DIR
   "${_releasedir@CMAKE_PROJECT_NAME@}/include" ABSOLUTE )
set( @CMAKE_PROJECT_NAME@_INCLUDE_DIRS ${@CMAKE_PROJECT_NAME@_INCLUDE_DIR} )

# Set the library directory variable:
get_filename_component( @CMAKE_PROJECT_NAME@_LIBRARY_DIR
   "${_releasedir@CMAKE_PROJECT_NAME@}/lib" ABSOLUTE )
set( @CMAKE_PROJECT_NAME@_LIBRARY_DIRS ${@CMAKE_PROJECT_NAME@_LIBRARY_DIR} )

# Set the runtime directory variable:
get_filename_component( @CMAKE_PROJECT_NAME@_RUNTIME_DIR
   "${_releasedir@CMAKE_PROJECT_NAME@}/bin" ABSOLUTE )
set( @CMAKE_PROJECT_NAME@_BINARY_PATH ${@CMAKE_PROJECT_NAME@_RUNTIME_DIR} )

# Set the python directory variable:
get_filename_component( @CMAKE_PROJECT_NAME@_PYTHON_DIR
   "${_releasedir@CMAKE_PROJECT_NAME@}/python" ABSOLUTE )
set( @CMAKE_PROJECT_NAME@_PYTHON_PATH ${@CMAKE_PROJECT_NAME@_PYTHON_DIR} )

# The base projects that this project was built on top of:
set( @CMAKE_PROJECT_NAME@_BASE_PROJECTS @ATLAS_BASE_PROJECTS@ )

# The platform name used for the build. It is overridden in release builds,
# but can be useful when building vanilla CMake code against the release.
set( ATLAS_PLATFORM @ATLAS_PLATFORM@ )

# Packages provided by the project:
set( @CMAKE_PROJECT_NAME@_PACKAGE_NAMES @ATLAS_EXPORTED_PACKAGES@ )

# Include the project-specific pre-include file, if it exists:
if( EXISTS ${_thisdir@CMAKE_PROJECT_NAME@}/PreConfig.cmake )
   include( ${_thisdir@CMAKE_PROJECT_NAME@}/PreConfig.cmake )
endif()

# Include the base projects, in the order in which they were given to
# the atlas_projects call.
set( _baseProjects@CMAKE_PROJECT_NAME@
   ${@CMAKE_PROJECT_NAME@_BASE_PROJECTS} )
while( _baseProjects@CMAKE_PROJECT_NAME@ )
   # Extract the release name and version, and then remove these entries
   # from the list:
   list( GET _baseProjects@CMAKE_PROJECT_NAME@ 0
      _base_project@CMAKE_PROJECT_NAME@ )
   list( GET _baseProjects@CMAKE_PROJECT_NAME@ 1
      _base_version@CMAKE_PROJECT_NAME@ )
   list( REMOVE_AT _baseProjects@CMAKE_PROJECT_NAME@ 0 1 )
   # Make sure that the project version is a regular version number:
   if( NOT _base_version@CMAKE_PROJECT_NAME@ MATCHES "^[0-9]+[0-9.]*" )
      # Let's not specify a version in this case...
      message( STATUS "Using base project "
         "\"${_base_project@CMAKE_PROJECT_NAME@}\" without "
         "its \"${_base_version@CMAKE_PROJECT_NAME@}\" version name/number" )
      set( _base_version@CMAKE_PROJECT_NAME@ )
   endif()
   # Find the base release:
   if( @CMAKE_PROJECT_NAME@_FIND_QUIETLY )
      find_package( ${_base_project@CMAKE_PROJECT_NAME@}
         ${_base_version@CMAKE_PROJECT_NAME@} QUIET )
   else()
      find_package( ${_base_project@CMAKE_PROJECT_NAME@}
         ${_base_version@CMAKE_PROJECT_NAME@} )
   endif()
endwhile()
unset( _baseProjects@CMAKE_PROJECT_NAME@ )
unset( _base_project@CMAKE_PROJECT_NAME@ )
unset( _base_version@CMAKE_PROJECT_NAME@ )

# Make CMake find the release's installed modules. Append the module
# library instead of prepending it. To allow the user to override the
# modules packaged with the release.
get_filename_component( @CMAKE_PROJECT_NAME@_MODULE_DIR
   "${_thisdir@CMAKE_PROJECT_NAME@}/modules" ABSOLUTE )
if( ATLAS_DONT_PREPEND_PROJECT_MODULES )
   list( APPEND CMAKE_MODULE_PATH ${@CMAKE_PROJECT_NAME@_MODULE_DIR} )
else()
   list( INSERT CMAKE_MODULE_PATH 0 ${@CMAKE_PROJECT_NAME@_MODULE_DIR} )
endif()
list( REMOVE_DUPLICATES CMAKE_MODULE_PATH )

# Pull in the ATLAS code:
include( AtlasFunctions )

# Include the file listing all the imported targets and options:
if( EXISTS
   ${_thisdir@CMAKE_PROJECT_NAME@}/@CMAKE_PROJECT_NAME@Config-targets.cmake )
   include(
      ${_thisdir@CMAKE_PROJECT_NAME@}/@CMAKE_PROJECT_NAME@Config-targets.cmake )
endif()

# Check what build mode the release was built with. And set CMAKE_BUILD_TYPE
# to that value by default. While there should only be one build mode in
# a given install area, provide an explicit preference order to the different
# build modes:
foreach( _type Debug RelWithDebInfo Release MinSizeRel Default )
   string( TOLOWER "${_type}" _typeLower )
   set( _fileName "${_thisdir@CMAKE_PROJECT_NAME@}/" )
   set( _fileName "${_fileName}@CMAKE_PROJECT_NAME@Config-targets" )
   set( _fileName "${_fileName}-${_typeLower}.cmake" )
   if( EXISTS "${_fileName}" AND NOT CMAKE_BUILD_TYPE )
      set( CMAKE_BUILD_TYPE "${_type}"
         CACHE STRING "Build mode for the release" FORCE )
   endif()
   unset( _fileName )
   unset( _typeLower )
endforeach()

# Tell the user how many packages were found:
if( NOT @CMAKE_PROJECT_NAME@_FIND_QUIETLY )
   list( LENGTH @CMAKE_PROJECT_NAME@_PACKAGE_NAMES _nPackages )
   message( STATUS "Package(s) in @CMAKE_PROJECT_NAME@: ${_nPackages}" )
   unset( _nPackages )
endif()

# If the libraries need to be set up...
if( @CMAKE_PROJECT_NAME@_FIND_COMPONENTS )

   # A sanity check.
   if( NOT ${@CMAKE_PROJECT_NAME@_FIND_COMPONENTS} STREQUAL "INCLUDE" )
      message( WARNING "Only the 'INCLUDE' component is understood.\n"
         "Continuing as if 'INCLUDE' would have been specified..." )
   endif()

   # Tell the user what's happening.
   message( STATUS "Including the packages from project "
      "@CMAKE_PROJECT_NAME@ - @CMAKE_PROJECT_VERSION@..." )

   # Targets to pull into the current project.
   set( @CMAKE_PROJECT_NAME@_TARGET_NAMES @ATLAS_EXPORTED_TARGETS@ )

   # Loop over the targets that this project has.
   foreach( _target ${@CMAKE_PROJECT_NAME@_TARGET_NAMES} )
      # If the target exists already, then don't do aything else.
      if( TARGET ${_target} )
         continue()
      endif()
      # Check whether the target in question is known in this release.
      if( NOT TARGET @CMAKE_PROJECT_NAME@::${_target} )
         message( SEND_ERROR
            "Target with name @CMAKE_PROJECT_NAME@::${_target} not found" )
         continue()
      endif()
      # Get the type of this target.
      get_property( _type TARGET @CMAKE_PROJECT_NAME@::${_target}
         PROPERTY TYPE )
      # And now create a copy of this target in the right way:
      if( "${_type}" STREQUAL "SHARED_LIBRARY" OR
            "${_type}" STREQUAL "MODULE_LIBRARY" OR
            "${_type}" STREQUAL "INTERFACE_LIBRARY" OR
            "${_type}" STREQUAL "STATIC_LIBRARY" OR
            "${_type}" STREQUAL "OBJECT_LIBRARY" )
         # First, create the right type of library:
         if( "${_type}" STREQUAL "SHARED_LIBRARY" )
            # Create a shared library:
            add_library( ${_target} SHARED IMPORTED GLOBAL )
         elseif( "${_type}" STREQUAL "MODULE_LIBRARY" )
            # Create a module library:
            add_library( ${_target} MODULE IMPORTED GLOBAL )
         elseif( "${_type}" STREQUAL "INTERFACE_LIBRARY" )
            # Create an interface library:
            add_library( ${_target} INTERFACE IMPORTED GLOBAL )
         elseif( "${_type}" STREQUAL "STATIC_LIBRARY" )
            # Create a static library:
            add_library( ${_target} STATIC IMPORTED GLOBAL )
         elseif( "${_type}" STREQUAL "OBJECT_LIBRARY" )
            # Create an object library:
            add_library( ${_target} OBJECT IMPORTED GLOBAL )
         else()
            message( FATAL_ERROR "Internal coding error found!" )
         endif()
         # Copy its dependencies:
         get_property( _libSet TARGET @CMAKE_PROJECT_NAME@::${_target}
            PROPERTY INTERFACE_LINK_LIBRARIES SET )
         if( _libSet )
            get_target_property( _libraries @CMAKE_PROJECT_NAME@::${_target}
               INTERFACE_LINK_LIBRARIES )
            string( REPLACE "@CMAKE_PROJECT_NAME@::" "" _libraries
               "${_libraries}" )
            set_target_properties( ${_target} PROPERTIES
               INTERFACE_LINK_LIBRARIES "${_libraries}" )
            unset( _libraries )
         endif()
         unset( _libSet )
         get_property( _incSet TARGET @CMAKE_PROJECT_NAME@::${_target}
            PROPERTY INTERFACE_SYSTEM_INCLUDE_DIRECTORIES SET )
         if( _incSet )
            get_target_property( _includes @CMAKE_PROJECT_NAME@::${_target}
               INTERFACE_SYSTEM_INCLUDE_DIRECTORIES )
            string( REPLACE "@CMAKE_PROJECT_NAME@::" "" _includes
               "${_includes}" )
            set_target_properties( ${_target} PROPERTIES
               INTERFACE_SYSTEM_INCLUDE_DIRECTORIES "${_includes}" )
            unset( _includes )
         endif()
         get_property( _incSet TARGET @CMAKE_PROJECT_NAME@::${_target}
            PROPERTY INTERFACE_INCLUDE_DIRECTORIES SET )
         if( _incSet )
            get_target_property( _includes @CMAKE_PROJECT_NAME@::${_target}
               INTERFACE_INCLUDE_DIRECTORIES )
            string( REPLACE "@CMAKE_PROJECT_NAME@::" "" _includes
               "${_includes}" )
            set_target_properties( ${_target} PROPERTIES
               INTERFACE_INCLUDE_DIRECTORIES "${_includes}" )
            unset( _includes )
         endif()
         unset( _incSet )
         get_property( _defSet TARGET @CMAKE_PROJECT_NAME@::${_target}
            PROPERTY INTERFACE_COMPILE_DEFINITIONS SET )
         if( _defSet )
            get_target_property( _definitions
               @CMAKE_PROJECT_NAME@::${_target}
               INTERFACE_COMPILE_DEFINITIONS )
            set_target_properties( ${_target} PROPERTIES
               INTERFACE_COMPILE_DEFINITIONS "${_definitions}" )
            unset( _definitions )
         endif()
         unset( _defSet )
         get_property( _optsSet TARGET @CMAKE_PROJECT_NAME@::${_target}
            PROPERTY INTERFACE_COMPILE_OPTIONS SET )
         if( _optsSet )
            get_target_property( _options
               @CMAKE_PROJECT_NAME@::${_target}
               INTERFACE_COMPILE_OPTIONS )
            set_target_properties( ${_target} PROPERTIES
               INTERFACE_COMPILE_OPTIONS "${_options}" )
            unset( _options )
         endif()
         unset( _optsSet )
         # For physical libraries, copy their build dependent properties as
         # well.
         get_property( _confSet
            TARGET @CMAKE_PROJECT_NAME@::${_target}
            PROPERTY IMPORTED_CONFIGURATIONS SET )
         if( _confSet )
            # Get the configurations that it is available for:
            get_target_property( _builds @CMAKE_PROJECT_NAME@::${_target}
               IMPORTED_CONFIGURATIONS )
            set_target_properties( ${_target} PROPERTIES
               IMPORTED_CONFIGURATIONS ${_builds} )
            foreach( _build ${_builds} )
               # Copy the properties:
               foreach( _propertyName IMPORTED_LOCATION_${_build}
                  IMPORTED_SONAME_${_build}
                  IMPORTED_LINK_DEPENDENT_LIBRARIES_${_build}
                  IMPORTED_LINK_INTERFACE_LANGUAGES_${_build}
                  IMPORTED_COMMON_LANGUAGE_RUNTIME_${_build}
                  IMPORTED_OBJECTS_${_build} )
                  get_property( _propSet
                     TARGET @CMAKE_PROJECT_NAME@::${_target}
                     PROPERTY ${_propertyName} SET )
                  if( _propSet )
                     get_target_property( _property
                        @CMAKE_PROJECT_NAME@::${_target} ${_propertyName} )
                     set_target_properties( ${_target} PROPERTIES
                        ${_propertyName} "${_property}" )
                     unset( _property )
                  endif()
                  unset( _propSet )
               endforeach()
            endforeach()
            unset( _builds )
         endif()
         unset( _confSet )
      elseif( "${_type}" STREQUAL "EXECUTABLE" )
         # Set up the imported executable:
         add_executable( ${_target} IMPORTED GLOBAL )
         # Copy its location for all available configurations:
         get_target_property( _builds @CMAKE_PROJECT_NAME@::${_target}
            IMPORTED_CONFIGURATIONS )
         set_target_properties( ${_target} PROPERTIES
            IMPORTED_CONFIGURATIONS "${_builds}" )
         foreach( _build ${_builds} )
            # Copy the properties:
            get_target_property( _location @CMAKE_PROJECT_NAME@::${_target}
               IMPORTED_LOCATION_${_build} )
            set_target_properties( ${_target} PROPERTIES
               IMPORTED_LOCATION_${_build} "${_location}" )
            unset( _location )
         endforeach()
         unset( _builds )
      else()
         message( WARNING "Unknown target type (${_type}) encountered" )
      endif()
      unset( _type )
   endforeach()

   # Include the base projects, in reverse order. So that the components from
   # the end of the list would get precedence over the components from the
   # front.
   set( _baseProjects@CMAKE_PROJECT_NAME@
      ${@CMAKE_PROJECT_NAME@_BASE_PROJECTS} )
   while( _baseProjects@CMAKE_PROJECT_NAME@ )
      # Get the last project from the list:
      list( LENGTH _baseProjects@CMAKE_PROJECT_NAME@
         _length@CMAKE_PROJECT_NAME@ )
      math( EXPR _projNameIdx@CMAKE_PROJECT_NAME@
         "${_length@CMAKE_PROJECT_NAME@} - 2" )
      math( EXPR _projVersIdx@CMAKE_PROJECT_NAME@
         "${_length@CMAKE_PROJECT_NAME@} - 1" )
      list( GET _baseProjects@CMAKE_PROJECT_NAME@
         ${_projNameIdx@CMAKE_PROJECT_NAME@}
         _base_project@CMAKE_PROJECT_NAME@ )
      list( GET _baseProjects@CMAKE_PROJECT_NAME@
         ${_projVersIdx@CMAKE_PROJECT_NAME@}
         _base_version@CMAKE_PROJECT_NAME@ )
      list( REMOVE_AT _baseProjects@CMAKE_PROJECT_NAME@
         ${_projNameIdx@CMAKE_PROJECT_NAME@}
         ${_projVersIdx@CMAKE_PROJECT_NAME@} )
      # Find the base release:
      if( "${@CMAKE_PROJECT_NAME@_FIND_COMPONENTS}" STREQUAL "INCLUDE" )
         find_package( ${_base_project@CMAKE_PROJECT_NAME@}
            COMPONENTS INCLUDE QUIET )
      endif()
   endwhile()
   unset( _baseProjects@CMAKE_PROJECT_NAME@ )
   unset( _projNameIdx@CMAKE_PROJECT_NAME@ )
   unset( _projVersIdx@CMAKE_PROJECT_NAME@ )
   unset( _base_project@CMAKE_PROJECT_NAME@ )
   unset( _base_version@CMAKE_PROJECT_NAME@ )
   unset( _length@CMAKE_PROJECT_NAME@ )

endif()

# Only do this if necessary:
if( NOT ATLAS_DONT_PREPEND_PROJECT_MODULES )
   # Make sure that after all of this, we still have this release's module
   # directory at the front of the list:
   list( INSERT CMAKE_MODULE_PATH 0 ${@CMAKE_PROJECT_NAME@_MODULE_DIR} )
   list( REMOVE_DUPLICATES CMAKE_MODULE_PATH )
endif()

# Include the project-specific post-include file, if it exists:
if( EXISTS ${_thisdir@CMAKE_PROJECT_NAME@}/PostConfig.cmake )
   include( ${_thisdir@CMAKE_PROJECT_NAME@}/PostConfig.cmake )
endif()

# Remove the unwanted variables:
unset( _thisdir@CMAKE_PROJECT_NAME@ )
unset( _releasedir@CMAKE_PROJECT_NAME@ )
