# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# This module collects functions used just for the ATLAS-style build of Gaudi.
#

# Provide a function for setting up CTest correctly for the Gaudi build.
#
# Usage: gaudi_ctest_setup()
#
macro( gaudi_ctest_setup )

   # First call the basic setup:
   atlas_ctest_setup()

endmacro( gaudi_ctest_setup )
